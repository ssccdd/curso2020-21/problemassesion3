﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 3

Problemas propuestos para la Sesión 3 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2020-21.

Los ejercicios son diferentes para cada grupo:

Los **objetivos** de la práctica son:

-   Definir adecuadamente las clases que implementan lo que se ejcutará en los _hilos_. Programando adecuadamente la excepción de interrupción de su ejecución.
- Utilizar adecuadamente la herramienta de sincronización propuesta para la resolución del ejercicio. Solo estará permitido utilizar objetos de esa herramienta y ningún otro.
-   Modificar el método `main(..)` de la aplicación Java para una prueba correcta de las clases previamente definidas.
    -   Crear los objetos que representan lo que deberá ejecutarse en los _hilos_.
    -   Crear los objetos de los _hilos_ de ejecución. Asociando correctamente lo que deberá ejercutarse.
    -   La interrupción de los _hilos_ cuando sea solicitado.

Los ejercicios son diferentes para cada grupo:

-   [Grupo 1](https://gitlab.com/ssccdd/curso2020-21/problemassesion3/-/blob/master/README.md#grupo-1)
-   [Grupo 2](https://gitlab.com/ssccdd/curso2020-21/problemassesion3/-/blob/master/README.md#grupo-2)
-   [Grupo 3](https://gitlab.com/ssccdd/curso2020-21/problemassesion3/-/blob/master/README.md#grupo-3)
-   [Grupo 4](https://gitlab.com/ssccdd/curso2020-21/problemassesion3/-/blob/master/README.md#grupo-4)
-   [Grupo 5](https://gitlab.com/ssccdd/curso2020-21/problemassesion3/-/blob/master/README.md#grupo-5)
-   [Grupo 6](https://gitlab.com/ssccdd/curso2020-21/problemassesion3/-/blob/master/README.md#grupo-6)

### Grupo 1
Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `Ordenador`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. La herramienta de sincronización que se utilizará en el ejercicio es [`CyclicBarrier`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CyclicBarrier.html) para las diferentes tareas que compondrán el ejercicio.

Hay que completar la implementación de las siguientes clases:

 - `CrearProcesos` : Esta tarea se encargará de ir creando procesos y tiene las siguientes variables de instancia:
	 - Un identificador.
	 - Un `TipoProceso` para los procesos que creará.
	 - Una lista donde almacenará los procesos que creará.
	 - Objeto de sincronización para iniciar un ciclo de producción.
	 - Objeto de sincronización para indicar que ha completado un `Proceso`.

	La tarea que tiene que realizar es:
	 - Mientras no se solicite su interrupción:
		 - Esperará hasta que todas las tareas `CrearProcesos` relacionadas con un `GestorMemoria` antes de comenzar con el siguiente ciclo para crear procesos.
		 - Crea un proceso y lo almacena en la lista.
		 - Se simulará un tiempo de creación definido por las constantes.
		 - Notificará la creación del proceso al gestor y espera hasta que todas las tareas alcancen este punto.

- `GestorMemoria` : Es el encargado de asignar un `Proceso` a un ordenador de los que tiene asignados. La clase tiene las siguientes variables de instancia:
	-   Un identificador
	-   Lista de ordenadores asignados.
	-   Lista de procesos no asignados a ningún ordenador.
	- Objeto de sincronización para indicar su finalización.

	La tarea que debe realizar es la siguiente:
	- Crear un `CrearProceso` para cada uno de los `TipoProceso` disponibles y comienza su ejecución.
		- Ciclo de asignación de procesos hasta que se haya alcanzado el máximo de fallos indicados en las constantes:
			- Antes de comenzar se sincronizará con sus tareas `CrearProceso` para que comiencen a crear procesos.
			- Esperará hasta que todos las tareas `CrearProceso` hayan creado un proceso cada uno.
			- Para cada `Proceso`, se elimina de la lista, se buscará un ordenador donde asignarlo, hay que procurar que la carga de procesos esté balanceada entre los diferentes ordenadores. Se simulará un tiempo aleatorio, atendiendo al tipo de proceso, para la realización de esta operación.
			- Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados. Esto cuenta como un fallo.
	- Programar la interrupción completando la última asignación de proceso.
	- Antes de finalizar interrumpe la ejecución de sus tareas `CrearProcesos` asociadas al gestor y lo notificará al hilo principal mediante el objeto de sincronización.

- `Hilo Principal` Deberá completar los siguientes pasos:
	- Crea un número de gestores de memoria que estarán determinados por una constante.
		- Cada gestor tendrá un valor variable de ordenadores asignados entre 2 y 5.
	-  Crea los hilos asociados a los gestores de memoria y comienza su ejecución.
	- Esperará hasta que uno de los gestores finalice. 
	- Solicita la interrupción del resto de gestores.
	- Presentará un informe donde se presente la asignación que cada uno de los gestores ha hecho de los procesos asociados a sus ordenadores.
	- Además se presentará la lista de todos los procesos no asignados por cada uno de los gestores.

### Grupo 2
Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `ColaPrioridad`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. La herramienta de sincronización que se utilizará en el ejercicio es [`CountDownLatch`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CountDownLatch.html) para las diferentes tareas que compondrán el ejercicio.

Hay que completar la implementación de las siguientes clases:
 - `CrearProcesos` : Esta tarea se encargará de ir creando procesos y tiene las siguientes variables de instancia:
	 - Un identificador.
	 - Una lista donde almacenará el proceso creado.
	 - Objeto de sincronización para iniciar la creación.
	 - Objeto de sincronización para indicar que se ha completado la operación.

	La tarea que tiene que realizar es:
	 - Esperará hasta que todas las tareas `CrearProcesos` relacionadas con un `GestorProcesos` antes de comenzar con la creación de un proceso.
	 - Se crea un `Proceso` aleatorio y se simulará un tiempo de creación utilizando las constantes.
	 - Almacena el proceso en la lista.
	 - Notificará la creación del proceso al gestor.

	Se debe programar la interrupción de la tarea si es solicitada.

- `GestorProcesos` : Es el encargado de asignar un `Proceso` a una cola de prioridad. La clase tiene las siguientes variables de instancia:
	-   Un identificador
	-   Lista de colas de prioridad asignadas.
	-   Lista de procesos no asignados a ninguna cola de prioridad.
	- Objeto de sincronización para indicar su finalización.

	La tarea que debe realizar es la siguiente:
	- Estaremos repitiendo la tarea hasta que se produzca un número de fallos establecido en las constantes.
		- Crear un número variable de tareas `CrearProcesos` y se notificará que pueden empezar a producir.
		- Esperará hasta que todos las tareas de creación hayan finalizado.
		- Para cada uno de los procesos creados se buscará una cola de prioridad donde asignarlo, hay que procurar que la carga de procesos esté balanceada. Se simulará un tiempo aleatorio para la realización de esta operación mediante las constantes.
		- Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados. Esto cuenta como un fallo.
	- Programar la interrupción finalizando las tareas `CrearProcesos` asociadas.
	- La finalización deberá comunicarse al hilo principal mediante el objeto de sincronización.

- `Hilo Principal` Deberá completar los siguientes pasos:
	- Crea un número de gestores de procesos que estarán determinados por una constante.
		- Cada gestor tendrá un valor variable de listas de prioridad asignados entre 2 y 5.
	-  Crea los hilos asociados a los gestores y comienza su ejecución.
	- Esperará hasta que la mitad de los gestores finalice. 
	- Solicita la interrupción del resto de gestores.
	- Presentará un informe donde se presente la asignación que cada uno de los gestores ha hecho de los procesos asociados a sus colas de prioridad.
	- Además se presentará la lista de todos los procesos no asignados por cada uno de los gestores.

### Grupo 3
Para la realización del ejercicio se hará uso de las clases ya definidas: `Archivo`, `UnidadAlmacenamiento`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. La herramienta de sincronización que se utilizará en el ejercicio es [`CyclicBarrier`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CyclicBarrier.html) para las diferentes tareas que compondrán el ejercicio.

Hay que completar la implementación de las siguientes clases:

 - `CrearArchivos` : Esta tarea se encargará de ir creando archivos y tiene las siguientes variables de instancia:
	 - Un identificador.
	 - Una lista donde almacenará los archivos que creará.
	 - Objeto de sincronización para iniciar un ciclo de producción.
	 - Objeto de sincronización para indicar que ha completado un `Archivo`.

	La tarea que tiene que realizar es:
	 - Mientras no se solicite su interrupción:
		 - Esperará hasta que todas las tareas `CrearArchivoss` relacionadas con un `GestorAlmacenamiento` antes de comenzar con el siguiente ciclo para crear el archivo.
		 - Crea un archivo de tipo aleatorio y lo almacena en la lista.
		 - Se simulará un tiempo de creación definido por las constantes.
		 - Notificará la creación del archivo al gestor y espera hasta que todas las tareas alcancen este punto.

- `GestorAlmacenamiento` : Es el encargado de asignar un `Archivo` a una de las unidades de almacenamientos que tiene asignadas. La clase tiene las siguientes variables de instancia:
	-   Un identificador
	-   Lista de unidades de almacenamiento asignadas.
	-   Lista de archivos no asignados a ninguna unidad de almacenamiento.
	- Objeto de sincronización para indicar su finalización.

	La tarea que debe realizar es la siguiente:
	- Crear un número variable de tareas `CrearArchivos` y comienza su ejecución.
		- Ciclo de asignación de archivos hasta que se haya alcanzado el máximo de fallos indicados en las constantes:
			- Antes de comenzar se sincronizará con sus tareas `CrearArchivos` para que comiencen a crearlos.
			- Esperará hasta que todos las tareas `CrearArchivos` hayan creado uno cada una.
			- Para cada `Archivo`, se elimina de la lista, se buscará la primera unidad de almacenamiento disponible donde incluirlo. Se simulará un tiempo aleatorio, atendiendo al tipo de proceso, para la realización de esta operación.
			- Si no ha podido ser asignado el archivo se añadirá a la lista de no asignados. Esto cuenta como un fallo.
	- Programar la interrupción completando la última asignación de archivos.
	- Antes de finalizar interrumpe la ejecución de sus tareas `CrearArchivos` asociadas al gestor y lo notificará al hilo principal mediante el objeto de sincronización.

- `Hilo Principal` Deberá completar los siguientes pasos:
	- Crea un número de gestores de almacenamiento que estarán determinados por una constante.
		- Cada gestor tendrá un valor variable de unidades de almacenamiento asignadas entre 2 y 5.
	-  Crea los hilos asociados a los gestores de almacenamiento y comienza su ejecución.
	- Esperará hasta que uno de los gestores finalice. 
	- Solicita la interrupción del resto de gestores.
	- Presentará un informe donde se presente la asignación que cada uno de los gestores ha realizado en sus unidades de almacenamiento.
	- Además se presentará la lista de todos los archivos no asignados por cada uno de los gestores.
	
	## Grupo 4
El ejercicio consiste en la preparación de los menús para repartir, la preparación se hará en fases, en cada una de ellas se pedirá la elaboración de cada tipo de plato a un restaurante, un restaurante servirá solo un tipo de plato para un menú concreto. En cada etapa, los repartidores pasarán cada pedido a un restaurante para que repare un plato y lo añada al pedido, al final de cada etapa los repartidores se sincronizarán para dar comienzo a la siguiente usando la herramienta [`Phaser`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Phaser.html). Además, para dar por terminada la preparación de menús, en cada etapa los repartidores crearán un [`CountDownLatch`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CountDownLatch.html) que registrará cuando un restaurante ha terminado. Para la solución **solo se pueden usar las herramientas de sincronización anteriores** y los siguientes elementos ya programados:

- `Utils` : Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
	- **Nota**: el enumerado TipoPlato tiene una nueva función que devuelve un TipoPlato dependiendo de su posición u ordinal.
- `Plato`: Clase que representa a un plato de tipo `TipoPlato`.
- `MenuReparto`: Representa a un pedido, puede incluir un plato de cada uno de los presentes en `TipoPlato` pero no más de uno de cada tipo.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `Restaurante`: Tiene un identificador, un TipoPlato que puede producir, un menú para introducir el plato y los elementos de sincronización necesarios.
	 - Para el plato que debe crear:
		 - Al empezar debe esperar un tiempo entre `TIEMPO_ESPERA_MIN` y `TIEMPO_ESPERA_MAX`.
		 - Creará un plato de su mismo tipo y lo insertará en el menú. El identificador del plato será un número entero positivo.
		 - Llamará a la función de sincronización para confirmar que ha terminado.
	 - Debe programarse un procedimiento de interrupción, tal que, si se interrumpe la espera inicial, no insertará el plato y acabará su ejecución.
- `Repartidor`: Tiene un identificador, una serie de `MenuReparto` para completar y los elementos de sincronización necesarios.
	- Antes de empezar deberá sincronizarse con el resto de repartidores, esta sincronización puede ser no interrumpible.
	 - Para cada una de las etapas tiene que cumplir:
		 - En cada etapa se rellenará un tipo de plato diferente en todos los menús, para eso se crearán tantos restaurantes como pedidos haya del tipo correspondiente, también se le incluirán los elementos necesarios de sincronización.
		 - Cada restaurante estará asociado a un hilo y se lanzará este.
		 - Antes de terminar la etapa el repartidor debe esperar a la finalización de los restaurantes.
		 - Al finalizar la etapa, los repartidores se sincronizarán, excepto en la última fase, donde los repartidores se desregistrarán.
	 - Cuando finalice el total de etapas o se interrumpa deberá darse de baja del elemento de sincronización con el resto de repartidores.
	 - Se tiene que programar un procedimiento de interrupción. Tiene que interrumpir a los restaurantes asignados.
	 - Debe presentar la lista de los menús que tiene asignados y si ha sido interrumpido debe de mostrar en qué etapa y cuantos restaurantes faltaban por terminar en dicha etapa.
- `Hilo principal`: Realizará las siguientes tareas:
	 - Creará las herramientas de sincronización.
	 - Creará un número de `Repartidores` igual a `REPARTIDORES_A_GENERAR`.
		 - Cada repartidor tendrá asignados `MENUS_A_GENERAR` pedidos.
		 - Se le asociará un hilo para su ejecución.
	 - Se ejecutarán todos los hilos.
	 - Se esperará un `TIEMPO_ESPERA_HILO_PRINCIPAL`, en segundos.
	 - A la finalización de la espera se solicitará la interrupción del trabajo de los repartidores.
	 - El hilo principal no esperará la finalización de los repartidores.

## Grupo 5
El ejercicio consiste en la impresión de una serie de modelos que posteriormente se postprocesarán, la impresión se hará en ciclos, en cada uno de ellos se imprimirá un modelo de cada impresora y se guardarán en una estructura de datos común para todos los ingenieros y para la máquina de procesado. Al final de cada ciclo los repartidores se sincronizarán para dar comienzo al siguiente usando la herramienta [`CyclicBarrier`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CyclicBarrier.html). Además, para dar lanzar la máquina de postprocesado se usará el constructor del `CyclicBarrier` que permite ejecutar un runnable cada vez que todos los hilos se sincronizan en un punto. El [`ReentrantLock`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/ReentrantLock.html) se nos asegurará que se accede de forma segura a la lista común. Para la solución **solo se pueden usar las herramientas de sincronización anteriores** y los siguientes elementos ya programados:

- `Utils` : Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
	- **Nota**: los tiempos de espera se representan en milisegundos.
- `Modelo`: Clase que representa a un modelo 3D con unos requisitos de tipo `CalidadImpresion`.
- `Impresora3D`: Representa a una impresora 3D y una `CalidadImpresion` que puede generar la impresora. 
	- **Nota**: para esta sesión se ha quitado la capacidad de la impresora de guardar modelos a la espera.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `MaquinaPostProcesado`: Este objeto se ejecutará automáticamente cuando todos los ingenieros se sincronicen al final de cada ciclo. Tiene una lista compartida con todos los ingenieros donde se guardan los modelos recién impresos y otra lista donde se guardan los modelos ya procesados a la que tendrá acceso también el hilo principal.
	 - Cuando es llamado tiene que:
		 - Mostrar por pantalla que ha sido invocado.
		 - Mientras haya modelos en la lista de recién impresos, cogerá el primer modelo, lo eliminará de la lista, esperará `TIEMPO_ESPERA_HILO_POSPROCESO` milisegundos y lo guardará en la lista de modelos finalizados.
		 - Mostrar por pantalla que ha terminado y cuantos modelos ha post procesado.
- `Ingeniero`: Tiene un identificador, una lista de impresoras asignadas, una lista de modelos recién impresos y los elementos de sincronización necesarios.
	 - Se creará un bucle que solo se puede interrumpir externamente y cada uno de los ciclos tiene que cumplir:
		 - En cada etapa se recorrerán las impresoras disponibles y se generará un modelo de cada impresora, este modelo se guardará en una estructura temporal.
		 - Antes de generar cada modelo, el ingeniero simulará el tiempo de impresión esperando entre `TIEMPO_REQUERIDO_MIN` y `TIEMPO_REQUERIDO_MAX` milisegundos, de tal forma que si se interrumpe durante la espera el modelo no se añade a la lista temporal.
		 - Antes de terminar el ciclo el ingeniero debe guardar los modelos de la lista temporal en la lista de modelos recién impresos.
		 - Cuando termine las tareas del ciclo, el ingeniero imprimirá que ha terminado el ciclo y esperará al resto de ingenieros, en el último 
	 - Cuando se interrumpa el trabajo se mostrarán cuantos ciclos ha terminado y cuantos modelos se han procesado durante el último ciclo.
	 - Se tiene que programar un procedimiento de interrupción.
- `Hilo principal`: Realizará las siguientes tareas:
	 - Creará las estructuras de datos compartidas:
		 - La lista de modelos recién impresos que se compartirá con los ingenieros y la máquina de postprocesado.
		 - La lista de modelos terminados que se compartirá con la máquina de postprocesado y el hilo principal.
	 - Creará la máquina de postprocesado.
	 - Creará las herramientas de sincronización.
	 - Creará un número de `Ingenieros` igual a `INGENIEROS_A_GENERAR` .
		 - Cada ingeniero tendrá asignadas `IMPRESORAS_A_GENERAR`  impresoras.
		 - Se le asociará un hilo para su ejecución.
	 - Se ejecutarán todos los hilos.
	 - Se esperará un `TIEMPO_ESPERA_HILO_PRINCIPAL`.
	 - A la finalización de la espera se solicitará la interrupción del trabajo de los ingenieros.
	 - El hilo principal esperará la finalización de los ingenieros.
	 - Si se interrumpe un ingeniero mientras espera en la barrera de sincronización, no se ejecuta la tarea de la máquina de postprocesado, por lo que crearemos un hilo con la máquina, lo iniciamos y esperamos a que termine.
	 - Al terminar el hilo principal debe imprimir la información de los modelos finalizados.

## Grupo 6
El ejercicio consiste en la preparación de la vacunación en dos fases, una fase para cada una de la dosis necesaria para cada paciente. Cada enfermero solo administrará las dosis de un fabricante concreto, para conseguir esto cada enfermero solicitará tantas dosis, **en cada fase**, como pacientes tenga asignados al almacén. La petición al almacén se simulará con la creación de un Almacén por cada pedido, asignándole un array compartido para las dosis y un [`CountDownLatch`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CountDownLatch.html), de esta forma el almacén descontará uno para cada dosis generada y guardada, cuando llegue a 0 el enfermero se activará y tendrá las dosis disponibles. Además, para dar por terminada la cada etapa los enfermeros usarán un [`CyclicBarrier`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CyclicBarrier.html) que registrará cuando un enfermero ha terminado cada etapa y permitirá su sincronización. Para la solución **solo se pueden usar las herramientas de sincronización anteriores** y los siguientes elementos ya programados:

- `Utils` : Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
	- **Nota**: el enumerado TipoPlato tiene una nueva función que devuelve un TipoPlato dependiendo de su posición u ordinal.
- `DosisVacuna`: Clase que representa a cada dosis de la vacuna de un `FabricanteVacuna` determinado.
- `Paciente`: Representa a un paciente al que se le pueden administrar hasta dos dosis siempre que las dos sean del mismo fabricante.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `AlmacenMedico`: Tiene un identificador, el número de dosis y tipo de fabricante del que servirá las dosis, una lista para introducir dichas dosis y los elementos de sincronización necesarios.
	- Mientras queden dosis que generar.
	- Para cada dosis que debe servir:
		 - Al empezar debe esperar `TIEMPO_ESPERA_FABRICANTE` en milisegundos.
		 - Creará una dosis de su fabricante y lo insertará en la lista. El identificador de la dosis será un número entero positivo.
		 - Llamará a la función de sincronización para confirmar que ha insertado dicha dosis.
	 - Debe programarse un procedimiento de interrupción, tal que, si se interrumpe la espera inicial, no insertará la dosis y acabará su ejecución.
- `Enfermero`: Tiene un identificador, el fabricante de la vacuna que usará, una serie de `Pacientes` para vacunar y los elementos de sincronización necesarios.
	-  Antes de empezar deberá sincronizarse con el resto de repartidores, esta sincronización puede ser no interrumpible.
	 - Para cada una de las fases tiene que cumplir:
		 - En cada etapa se inyectará una dosis a cada paciente, por lo que son necesarias tantas dosis como pacientes. Para eso se creará un almacén, a este almacén se le suministrará el tipo de fabricante, cuantas dosis generar (una por cada paciente) y la lista para insertar las dosis, también se le incluirán los elementos necesarios de sincronización.
		 - Cada almacén estará asociado a un hilo y se lanzará este.
		 - Antes de terminar de empezar la vacunación el enfermero debe esperar a la finalización del almacén.
		 - Entre cada dosis el enfermero esperará `ESPERA_ENTRE_DOSIS` milisegundos.
		 - La dosis administrada debe eliminarse de la lista para poder reusarla.
		 - Al finalizar la etapa, los enfermeros se sincronizarán, excepto en la última fase.
	 - Se tiene que programar un procedimiento de interrupción. Tiene que interrumpir al hilo del almacén si está activo.
	 - Debe presentar la lista de los pacientes que tiene asignados y si ha sido interrumpido debe de mostrar en qué etapa y cuantas dosis han quedado sin usar en dicha fase.
- `Hilo principal`: Realizará las siguientes tareas:
	 - Creará un número de `Enfermeros` igual a `ENFERMEROS_A_GENERAR`.
		 - Cada repartidor tendrá asignados entre `PACIENTES_A_GENERAR_MIN` y `PACIENTES_A_GENERAR_MAX` pacientes.
		 - Se le asociará un hilo para su ejecución.
	 - Se ejecutarán todos los hilos.
	 - Se esperará un `TIEMPO_ESPERA_HILO_PRINCIPAL`, en milisegundos.
	 - A la finalización de la espera se solicitará la interrupción del trabajo de los enfermeros.
	 - El hilo principal no esperará la finalización de los repartidores.
