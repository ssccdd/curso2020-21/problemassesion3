/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.MINIMO;
import es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.TipoProceso;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Sesion3.getID;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class CrearProcesos implements Runnable {
    private final String iD;
    private final TipoProceso tipoProceso;
    private final ArrayList<Proceso> listaProcesos;
    private final CyclicBarrier inicioCreacion;
    private final CyclicBarrier finCreacion;

    public CrearProcesos(String iD, TipoProceso tipoProceso, ArrayList<Proceso> listaProcesos, 
                         CyclicBarrier inicioCreacion, CyclicBarrier finCreacion) {
        this.iD = iD;
        this.tipoProceso = tipoProceso;
        this.listaProcesos = listaProcesos;
        this.inicioCreacion = inicioCreacion;
        this.finCreacion = finCreacion;
    }

    
    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza su jornada de trabajo");        
        
        // Está creando procesos hasta que se solicite su interrupción
        // para finalizar su ejecución
        try {
            while ( true ) 
                crearProceso();
        } catch (InterruptedException | BrokenBarrierException ex) {
            System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Finaliza su jornada de trabajo");
        }

    }
    
    private void crearProceso() throws InterruptedException, BrokenBarrierException {
        // Antes de empear comprobamos si ha finalizado la creación
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        // Esperamos hasta que el gestor y los creadores estén sincronizados
        inicioCreacion.await();
        
        // Simula la creación de un proceso
        Proceso proceso = new Proceso(getID(),tipoProceso);
        listaProcesos.add(proceso);
        int tiempo = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        TimeUnit.SECONDS.sleep(tiempo);
        
        // Indica que se ha creado un proceso
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Crea el " + proceso);
        finCreacion.await();
    }

    public String getiD() {
        return iD;
    }
}
