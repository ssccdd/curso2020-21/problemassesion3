/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.MEMORIA_COMPLETA;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.NUM_FALLOS;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.TIPOS_PROCESO;
import es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.TipoProceso;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorMemoria implements Runnable {
    private final String iD;
    private final ArrayList<Ordenador> listaOrdenadores;
    private final ArrayList<Proceso> listaNoAsignados;
    private final CyclicBarrier finGestor;
    private int ultimoOrdenador; // último ordenador donde se asignó un proceso
    private int fallosAsignacion; 

    public GestorMemoria(String iD, ArrayList<Ordenador> listaOrdenadores, ArrayList<Proceso> listaNoAsignados, CyclicBarrier finGestor) {
        this.iD = iD;
        this.listaOrdenadores = listaOrdenadores;
        this.listaNoAsignados = listaNoAsignados;
        this.finGestor = finGestor;
        this.ultimoOrdenador = 0;
        this.fallosAsignacion = 0;
    }
    
    @Override
    public void run() {
        // CrearProcesos y sus hilos
        ArrayList<Proceso> listaProcesos;
        Thread[] hilos;
        CyclicBarrier inicioCreacion;
        CyclicBarrier finCreacion;
       
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza su ejecución");
        
        // Inicialización de las variables de trabajo
        inicioCreacion = new CyclicBarrier(TIPOS_PROCESO+1);
        finCreacion = new CyclicBarrier(TIPOS_PROCESO+1);
        hilos = new Thread[TIPOS_PROCESO];
        listaProcesos = new ArrayList();
        
        inicioCreadores(hilos,listaProcesos,inicioCreacion,finCreacion);
        
        try {
            // Hasta que se alcance el número de fallos o sea interrumpido
            while( fallosAsignacion < NUM_FALLOS ) {
                iniciarCreacion(inicioCreacion);
                finalizarCreacion(finCreacion, listaProcesos);
            }
            
            System.out.println("Ha finalizado la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } catch (InterruptedException | BrokenBarrierException ex) {
            System.out.println("Se ha interrumpido la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } finally {
            // Se le indica al hilo principal que se ha finalizado la ejecución
            // finalizando la ejecución de sus tareas asociadas
            finalizacion(hilos, listaProcesos);
        }
        
    }

    public String getiD() {
        return iD;
    }
    
    /**
     * Se crean y ejecutan las tareas asociadas al gestor
     * @param hilos
     * @param listaProcesos
     * @param inicioCreacion
     * @param finCreacion 
     */
    private void inicioCreadores(Thread[] hilos, ArrayList<Proceso> listaProcesos, 
                            CyclicBarrier inicioCreacion, CyclicBarrier finCreacion) {
        int i = 0;
        for(TipoProceso tipoProceso : TipoProceso.values()) {
            CrearProcesos tarea = new CrearProcesos(iD + "-tarea para proceso- " + tipoProceso.name(), tipoProceso, listaProcesos, 
                                                    inicioCreacion, finCreacion);
            hilos[i] = new Thread(tarea, tarea.getiD());
            hilos[i].start();
            i++;
        }
    }
    
    /**
     * Sincronización con las tareas que crean los procesos y el gestor encargado
     * @param inicioCreacion
     * @throws InterruptedException
     * @throws BrokenBarrierException 
     */
    private void iniciarCreacion(CyclicBarrier inicioCreacion) throws InterruptedException, BrokenBarrierException {
        // Sincronizamos en el mismo todas las tareas con el gestor
        inicioCreacion.await();
        
        // Restauramos la sincronización para el siguiente ciclo
        inicioCreacion.reset();

    }
    
    /**
     * Se reparten los procesos almacenados en la lista por las tareas relacionadas
     * con el gestor
     * @param finCreacion
     * @param listaProcesos
     * @param fallo
     * @throws InterruptedException
     * @throws BrokenBarrierException 
     */
    private void finalizarCreacion(CyclicBarrier finCreacion, ArrayList<Proceso> listaProcesos) throws InterruptedException, BrokenBarrierException {
        // Esperamos a tener todos los procesos creados por las tareas asociadas
        finCreacion.await();
        
        // Restauramos la sincronización para el siguiente ciclo de creación
        finCreacion.reset();

        // Asignamos el proceso a un ordenador si es posible
        System.out.println("HILO-" + Thread.currentThread().getName() + "lista procesos " + listaProcesos);
        Iterator it = listaProcesos.iterator();
        while( it.hasNext() && (fallosAsignacion < NUM_FALLOS)) {
            // Comprueba la solicitud de interrupción de la tarea antes de comenzar
            // con la siguiente asignación de proceso.
            if ( Thread.interrupted() )
                throw new InterruptedException();
            Proceso proceso = (Proceso) it.next();
            it.remove(); // Eliminamos el proceso de la lista
            
            // Como la lista de procesos no está asegurada hay que comprobar
            // que el proceso es válido.
            if( proceso != null ) {
                System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Asignando " + proceso + " faltan " + listaProcesos.size());
                // Asignamos el proceso a un ordenador si es posible
                boolean asignado = false;
                int ordenadoresRevisados = 0;
                while( (ordenadoresRevisados < listaOrdenadores.size()) && !asignado ) {
                    int indice = (ultimoOrdenador + ordenadoresRevisados) % listaOrdenadores.size();
                    if(listaOrdenadores.get(indice).addProceso(proceso) != MEMORIA_COMPLETA)
                        asignado = true;
                    else
                        ordenadoresRevisados++;
                }
            
                // Si no se ha asignado se anota el proceso en otro caso se actualiza
                // el último ordenador asignado
                if(asignado)
                    ultimoOrdenador = (ultimoOrdenador + ordenadoresRevisados + 1) % listaOrdenadores.size();
                else {
                    listaNoAsignados.add(proceso);
                    fallosAsignacion++;
                }
            
                // Simulamos por último el tiempo de asignación porque si se interrumpe
                // ya hemos completado la operación de asignación
                TimeUnit.SECONDS.sleep(tiempoAsignacion(proceso.getTipoProceso()));
            }
        }
        
    }
    
    private void finalizacion(Thread[] hilos, ArrayList<Proceso> listaProcesos) {
        for( Thread hilo : hilos )
            hilo.interrupt();
        
        try {
            for( Thread hilo : hilos )
                hilo.join();
            
            // registramos los procesos no asignados
            for(Proceso proceso : listaProcesos)
                listaNoAsignados.add(proceso);
            
            finGestor.await();
        } catch (Exception ex) {
            // No se trata porque se está finalizando con la ejecución
        } 
    }
    
    private int tiempoAsignacion(TipoProceso tipoProceso) {
        return MAXIMO + tipoProceso.ordinal();
    }
}
