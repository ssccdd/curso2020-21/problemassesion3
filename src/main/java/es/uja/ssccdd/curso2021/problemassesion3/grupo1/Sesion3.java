/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo1;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.ESPERA_A_GESTOR;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.TIPOS_PROCESO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 *
 * @author pedroj
 */
public class Sesion3 {
    static int ID = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws BrokenBarrierException, InterruptedException {
        Thread[] listaHilos;
        GestorMemoria gestor;
        ArrayList<Ordenador>[] listaOrdenadores;
        ArrayList<Proceso>[] listaNoAsignados;
        CyclicBarrier finEjecucion;
        int[] capacidad; // Capacidad de almacenamiento para los ordenadores
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
    
        // Inicializamos las variables del sistema
        listaOrdenadores = new ArrayList[NUM_GESTORES];
        listaNoAsignados = new ArrayList[NUM_GESTORES];
        listaHilos = new Thread[NUM_GESTORES];
        finEjecucion = new CyclicBarrier(ESPERA_A_GESTOR+1);
        capacidad = new int[TIPOS_PROCESO];
        for(int i = 0; i < NUM_GESTORES; i++) {
            // Creamos las listas asociadas al gestor i
            listaOrdenadores[i] = new ArrayList();
            listaNoAsignados[i] = new ArrayList();
             
            // Ordenadores para el gestor i
            int totalOrdenadores = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            for(int j = 0; j < totalOrdenadores; j++) {
                // Generamos la capacidad del ordenador
                for(int k = 0; k < TIPOS_PROCESO; k++) 
                    capacidad[k] = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
            
                listaOrdenadores[i].add(new Ordenador(getID(),capacidad));
            }
            
            gestor = new GestorMemoria("GESTOR(" + i + ")", listaOrdenadores[i], listaNoAsignados[i], finEjecucion);
            // Asociamos el gestor a su hilo
            listaHilos[i] = new Thread(gestor, gestor.getiD());
        }
    
        // Ejecutamos los hilos
        for(Thread hilo : listaHilos)
            hilo.start();
        
        // Suspendemos el hilo principal por un tiempo establecido
        System.out.println("(HILO_PRINCIPAL) Espera a la finalización de un gestor");
        finEjecucion.await();

        // Se solicita la cancelación de los gestores que no han terminado
        System.out.println("(HILO_PRINCIPAL) Solicita la finalización de los gestores");
        for( Thread hilo : listaHilos ) 
            hilo.interrupt();
        
        // Espera a la finalización de todos los hilos
        for( Thread hilo : listaHilos )
            hilo.join();

        // Mostrar el estado de sistema distribuido
        System.out.println("(HILO_PRINCIPAL) Estado del sitema distribuido");
        
        for(int i = 0; i < NUM_GESTORES; i++) {
            System.out.println("******************** Gestor(" + i + ") ********************");
            for(Ordenador ordenador : listaOrdenadores[i])
                System.out.println(ordenador);
            
            System.out.println("Procesos no asignados " + listaNoAsignados[i]);
        }
        
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
    public static int getID() {
        return ID++;
    }
}
