/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.TipoPrioridad.getTipoPrioridad;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.aleatorio;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Sesion3.getID;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class CrearProcesos implements Runnable {
    private final String iD;
    private final ArrayList<Proceso> listaProcesos;
    private final CountDownLatch inicio;
    private final CountDownLatch fin;

    public CrearProcesos(String iD, ArrayList<Proceso> listaProcesos, CountDownLatch inicio, CountDownLatch fin) {
        this.iD = iD;
        this.listaProcesos = listaProcesos;
        this.inicio = inicio;
        this.fin = fin;
    }

    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza su ejecución");        
        
        // Se crea un proceso con el tipo de prioridad aleatorio
        try {
            
            crearProceso();
            
        } catch (InterruptedException ex) {
            System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Ha sido cancelada su ejecución");
        } finally {
            // Indica que ha completado su proceso de creación
            fin.countDown();
        }
    }
    
    private void crearProceso() throws InterruptedException {
        // Antes de empear comprobamos si ha finalizado la creación
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        // Esperamos hasta que el gestor y los creadores estén sincronizados
        inicio.await();
        
        // Simula la creación de un proceso
        Proceso proceso = new Proceso(getID(),getTipoPrioridad(VALOR_CONSTRUCCION));
        listaProcesos.add(proceso);
        int tiempo = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        TimeUnit.SECONDS.sleep(tiempo);
        
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Crea el " + proceso);
    }

    public String getiD() {
        return iD;
    }
}
