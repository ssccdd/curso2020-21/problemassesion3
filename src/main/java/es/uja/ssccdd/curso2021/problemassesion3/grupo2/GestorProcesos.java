/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.INICIO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.NO_ASIGNADO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.NUM_FALLOS;
import es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.TipoPrioridad;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo2.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorProcesos implements Runnable {
    private final String iD;
    private final ArrayList<ColaPrioridad> listaColasPrioridad;
    private final ArrayList<Proceso> listaNoAsignados;
    private final CountDownLatch finGestor;
    private int ultimaCola; // última cola de prioridad donde se asignó un proceso
    private int fallosAsignacion;

    public GestorProcesos(String iD, ArrayList<ColaPrioridad> listaColasPrioridad, ArrayList<Proceso> listaNoAsignados, CountDownLatch finGestor) {
        this.iD = iD;
        this.listaColasPrioridad = listaColasPrioridad;
        this.listaNoAsignados = listaNoAsignados;
        this.finGestor = finGestor;
        this.ultimaCola = 0;
        this.fallosAsignacion = 0;
    }

    @Override
    public void run() {
        ArrayList<Thread> hilos = new ArrayList();
        ArrayList<Proceso> listaProcesos = new ArrayList();
        
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza su ejecución");
        
        try {
            // Hasta que se alcance el número de fallos o sea interrumpido
            while( fallosAsignacion < NUM_FALLOS ) {
                asignarProcesos(hilos, listaProcesos);
            }
            
            System.out.println("Ha finalizado la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } finally {
            // Se le indica al hilo principal que se ha finalizado la ejecución
            // finalizando la ejecución de sus tareas asociadas
            finalizacion(hilos, listaProcesos);
        }
        
    }

    public String getiD() {
        return iD;
    }
    
    
    private void asignarProcesos(ArrayList<Thread> hilos, ArrayList<Proceso> listaProcesos) throws InterruptedException {
        CountDownLatch inicioTareas;
        CountDownLatch finTareas;
        
        // Creamos las tareas asociadas al gestor y se ejecutan
        int numProcesos = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        listaProcesos.clear();
        hilos.clear();
        inicioTareas = new CountDownLatch(INICIO);
        finTareas = new CountDownLatch(numProcesos);
        for(int i = 0; i < numProcesos; i++) {
            CrearProcesos tarea = new CrearProcesos(iD + "-tarea crear procesos- " + i, listaProcesos, 
                                                    inicioTareas, finTareas);
            hilos.add(new Thread(tarea, tarea.getiD()));
        }
        for(Thread hilo : hilos)
            hilo.start();
        
        // Sincronizamos en el mismo todas las tareas con el gestor
        inicioTareas.countDown();
    
        // Esperamos a tener todos los procesos creados por las tareas asociadas
        finTareas.await();
      
        // Asignamos el proceso a una cola si es posible
        System.out.println("HILO-" + Thread.currentThread().getName() + "lista procesos " + listaProcesos);
        Iterator it = listaProcesos.iterator();
        while( it.hasNext() && (fallosAsignacion < NUM_FALLOS)) {
            // Comprueba la solicitud de interrupción de la tarea antes de comenzar
            // con la siguiente asignación de proceso.
            if ( Thread.interrupted() )
                throw new InterruptedException();
            Proceso proceso = (Proceso) it.next();
            it.remove(); // Eliminamos el proceso de la lista
            
            // Como la lista de procesos no está asegurada hay que comprobar
            // que el proceso es válido.
            if( proceso != null ) {
                System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Asignando " + proceso + " faltan " + listaProcesos.size());
                // Asignamos el proceso a un ordenador si es posible
                boolean asignado = false;
                int colasRevisadas = 0;
                while( (colasRevisadas < listaColasPrioridad.size()) && !asignado ) {
                    int indice = (ultimaCola + colasRevisadas) % listaColasPrioridad.size();
                    if(listaColasPrioridad.get(indice).addProceso(proceso) != NO_ASIGNADO)
                        asignado = true;
                    else
                        colasRevisadas++;
                }
            
                // Si no se ha asignado se anota el proceso en otro caso se actualiza
                // la última cola revisada
                if(asignado)
                    ultimaCola = (ultimaCola + colasRevisadas + 1) % listaColasPrioridad.size();
                else {
                    listaNoAsignados.add(proceso);
                    fallosAsignacion++;
                }
            
                // Simulamos por último el tiempo de asignación porque si se interrumpe
                // ya hemos completado la operación de asignación
                TimeUnit.SECONDS.sleep(tiempoAsignacion(proceso.getTipoPrioridad()));
            }
        }
        
    }
    
    private void finalizacion(ArrayList<Thread> hilos, ArrayList<Proceso> listaProcesos) {
        for( Thread hilo : hilos )
            hilo.interrupt();
        
        try {
            for( Thread hilo : hilos )
                hilo.join();
            
            // registramos los procesos no asignados
            for(Proceso proceso : listaProcesos)
                listaNoAsignados.add(proceso);
            
            finGestor.countDown();
        } catch (Exception ex) {
            // No se trata porque se está finalizando con la ejecución
        } 
    }
    
    private int tiempoAsignacion(TipoPrioridad tipoPrioridad) {
        return MAXIMO + tipoPrioridad.ordinal();
    }
}
