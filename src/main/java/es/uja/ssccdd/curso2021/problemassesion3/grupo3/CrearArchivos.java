/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo3.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo3.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo3.Constantes.TipoArchivo.getTipoArchivo;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo3.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo3.Constantes.aleatorio;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo3.Sesion3.getID;
import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class CrearArchivos implements Runnable {
    private final String iD;
    private final ArrayList<Archivo> listaArchivos;
    private final CyclicBarrier inicioCreacion;
    private final CyclicBarrier finCreacion;

    public CrearArchivos(String iD, ArrayList<Archivo> listaArchivos, CyclicBarrier inicioCreacion, CyclicBarrier finCreacion) {
        this.iD = iD;
        this.listaArchivos = listaArchivos;
        this.inicioCreacion = inicioCreacion;
        this.finCreacion = finCreacion;
    }
    
    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza su jornada de trabajo");        
        
        // Está creando procesos hasta que se solicite su interrupción
        // para finalizar su ejecución
        try {
            while ( true ) 
                crearArchivo();
        } catch (InterruptedException | BrokenBarrierException ex) {
            System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Finaliza su jornada de trabajo");
        }
    }
    
    private void crearArchivo() throws InterruptedException, BrokenBarrierException {
        // Antes de empear comprobamos si ha finalizado la creación
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        // Esperamos hasta que el gestor y los creadores estén sincronizados
        inicioCreacion.await();
        
        // Simula la creación de un proceso
        Archivo archivo = new Archivo(getID(), getTipoArchivo(VALOR_CONSTRUCCION));
        listaArchivos.add(archivo);
        int tiempo = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        TimeUnit.SECONDS.sleep(tiempo);
        
        // Indica que se ha creado un proceso
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Crea el " + archivo);
        finCreacion.await();
    }

    public String getiD() {
        return iD;
    }
}
