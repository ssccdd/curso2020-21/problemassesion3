/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo3.Constantes.ESPACIO_INSUFICIENTE;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo3.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo3.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo3.Constantes.NUM_FALLOS;
import es.uja.ssccdd.curso2021.problemassesion3.grupo3.Constantes.TipoArchivo;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo3.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorAlmacenamiento implements Runnable {

    private final String iD;
    private final ArrayList<UnidadAlmacenamiento> listaDeAlmacenamiento;
    private final ArrayList<Archivo> listaNoAsignados;
    private final CyclicBarrier finGestor;
    private int fallosAsignacion;

    public GestorAlmacenamiento(String iD, ArrayList<UnidadAlmacenamiento> listaDeAlmacenamiento,
            ArrayList<Archivo> listaNoAsignados, CyclicBarrier finGestor) {
        this.iD = iD;
        this.listaDeAlmacenamiento = listaDeAlmacenamiento;
        this.listaNoAsignados = listaNoAsignados;
        this.finGestor = finGestor;
        this.fallosAsignacion = 0;
    }

    @Override
    public void run() {
        // CrearProcesos y sus hilos
        ArrayList<Archivo> listaArchivos;
        Thread[] hilos;
        CyclicBarrier inicioCreacion;
        CyclicBarrier finCreacion;

        System.out.println("HILO-" + Thread.currentThread().getName()
                + " Empieza su ejecución");

        // Inicialización de las variables de trabajo
        int numTareas = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        inicioCreacion = new CyclicBarrier(numTareas + 1);
        finCreacion = new CyclicBarrier(numTareas + 1);
        hilos = new Thread[numTareas];
        listaArchivos = new ArrayList();

        inicioCreadores(hilos, listaArchivos, inicioCreacion, finCreacion);

        try {
            // Hasta que se alcance el número de fallos o sea interrumpido
            while (fallosAsignacion < NUM_FALLOS) {
                iniciarCreacion(inicioCreacion);
                finalizarCreacion(finCreacion, listaArchivos);
            }

            System.out.println("Ha finalizado la ejecución del HILO-"
                    + Thread.currentThread().getName());
        } catch (InterruptedException | BrokenBarrierException ex) {
            System.out.println("Se ha interrumpido la ejecución del HILO-"
                    + Thread.currentThread().getName());
        } finally {
            // Se le indica al hilo principal que se ha finalizado la ejecución
            // finalizando la ejecución de sus tareas asociadas
            finalizacion(hilos, listaArchivos);
        }

    }

    public String getiD() {
        return iD;
    }

    /**
     * Se crean y ejecutan las tareas asociadas al gestor
     *
     * @param hilos
     * @param listaArchivos
     * @param inicioCreacion
     * @param finCreacion
     */
    private void inicioCreadores(Thread[] hilos, ArrayList<Archivo> listaArchivos,
            CyclicBarrier inicioCreacion, CyclicBarrier finCreacion) {

        for (int i = 0; i < hilos.length; i++) {
            CrearArchivos tarea = new CrearArchivos(iD + "-tareaArchivos- " + i, listaArchivos,
                    inicioCreacion, finCreacion);
            hilos[i] = new Thread(tarea, tarea.getiD());
            hilos[i].start();
        }
    }

    /**
     * Sincronización con las tareas que crean los archivos y el gestor
     * encargado
     *
     * @param inicioCreacion
     * @throws InterruptedException
     * @throws BrokenBarrierException
     */
    private void iniciarCreacion(CyclicBarrier inicioCreacion) throws InterruptedException, BrokenBarrierException {
        // Sincronizamos en el mismo todas las tareas con el gestor
        inicioCreacion.await();

        // Restauramos la sincronización para el siguiente ciclo
        inicioCreacion.reset();

    }

    /**
     * Se asignal los archivos creados a las unidades de almacenamiento con el
     * gestor
     *
     * @param finCreacion
     * @param listaArchivos
     * @param fallo
     * @throws InterruptedException
     * @throws BrokenBarrierException
     */
    private void finalizarCreacion(CyclicBarrier finCreacion, ArrayList<Archivo> listaArchivos) throws InterruptedException, BrokenBarrierException {
        // Esperamos a tener todos los archivos creados por las tareas asociadas
        finCreacion.await();

        // Restauramos la sincronización para el siguiente ciclo de creación
        finCreacion.reset();

        // Asignamos el archivos a la primera unidad disponible
        System.out.println("HILO-" + Thread.currentThread().getName() + "lista archivos " + listaArchivos);
        Iterator it = listaArchivos.iterator();
        while (it.hasNext() && (fallosAsignacion < NUM_FALLOS)) {
            // Comprueba la solicitud de interrupción de la tarea antes de comenzar
            // con la siguiente asignación de proceso.
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            Archivo archivo = (Archivo) it.next();
            it.remove(); // Eliminamos el archivo de la lista

            // Como la lista de archivos no está asegurada hay que comprobar
            // que el proceso es válido.
            if (archivo != null) {
                System.out.println("HILO-" + Thread.currentThread().getName()
                        + " Asignando " + archivo + " faltan " + listaArchivos.size());
                // Asignamos el proceso a un ordenador si es posible
                boolean asignado = false;
                int indice = 0;
                while ((indice < listaDeAlmacenamiento.size()) && !asignado) {
                    if (listaDeAlmacenamiento.get(indice).addArchivo(archivo) != ESPACIO_INSUFICIENTE) {
                        asignado = true;
                    } else {
                        indice++;
                    }
                }

                // Si no se ha podido almacenar el archivo se anota
                if(!asignado) {
                    listaNoAsignados.add(archivo);
                    fallosAsignacion++;
                }

                // Simulamos por último el tiempo de asignación porque si se interrumpe
                // ya hemos completado la operación de asignación
                TimeUnit.SECONDS.sleep(tiempoAsignacion(archivo.getTipoArchivo()));
            }
        }

    }

    private void finalizacion(Thread[] hilos, ArrayList<Archivo> listaArchivos) {
        for (Thread hilo : hilos) {
            hilo.interrupt();
        }

        try {
            for (Thread hilo : hilos) {
                hilo.join();
            }

            // registramos los procesos no asignados
            for (Archivo archivo : listaArchivos) {
                listaNoAsignados.add(archivo);
            }

            finGestor.await();
        } catch (Exception ex) {
            // No se trata porque se está finalizando con la ejecución
        }
    }

    private int tiempoAsignacion(TipoArchivo tipoArchivo) {
        return MAXIMO + tipoArchivo.ordinal();
    }
}
