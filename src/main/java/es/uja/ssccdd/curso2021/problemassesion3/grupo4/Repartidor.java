/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo4;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo4.Utils.TOTAL_TIPOS_PLATOS;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Phaser;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Repartidor implements Runnable {

    private final int iD;
    private final ArrayList<MenuReparto> pedidos;
    Phaser phaserRepartidores;

    public Repartidor(int iD, ArrayList<MenuReparto> pedidos, Phaser phaserRepartidores) {
        this.iD = iD;
        this.pedidos = pedidos;
        this.phaserRepartidores = phaserRepartidores;
    }

    @Override
    public void run() {

        phaserRepartidores.arriveAndAwaitAdvance();
        //Estas variables son solo para guardar el estado en la interrupciçon y mostrar los datos correctos.
        boolean interrumpido = false;
        int etapaActual = -1;
        int restaurantesPendientes = 0;

        for (int i = 0; i < TOTAL_TIPOS_PLATOS && !interrumpido; i++) {

            System.out.println(Thread.currentThread().getName() + " ha llegado al inicio de la fase " + i);

            ArrayList<Thread> hilosRestaurante = new ArrayList<>();
            //Los CountDownLatch no son reusables, se necesita uno cada vez
            CountDownLatch sincronizadorRestaurantes = new CountDownLatch(pedidos.size());

            for (int j = 0; j < pedidos.size(); j++) {
                hilosRestaurante.add(new Thread(new Restaurante(iD * 100 + i, Utils.TipoPlato.getPlatoOrdinal(i), sincronizadorRestaurantes, pedidos.get(j))));
            }

            for (Thread thread : hilosRestaurante) {
                thread.start();
            }

            try {

                sincronizadorRestaurantes.await();

                if (i == TOTAL_TIPOS_PLATOS - 1) {//Si es la última fase
                    
                    System.out.println(Thread.currentThread().getName() + " ha llegado al final de la última fase y no esperará al resto de los repartidores");
                    phaserRepartidores.arriveAndDeregister();
                } else {

                    System.out.println(Thread.currentThread().getName() + " ha llegado al final de la fase " + i + " esperando al resto de los repartidores");  
                    phaserRepartidores.awaitAdvanceInterruptibly(phaserRepartidores.arrive());
                }

            } catch (InterruptedException ex) {
                //Como todos los hilos van a ser interrumpidos, no es necesario, pero siempre es buena idea desregistrase
                phaserRepartidores.arriveAndDeregister();

                interrumpido = true;
                restaurantesPendientes = (int) sincronizadorRestaurantes.getCount();
                etapaActual = i;
                
                //Por si quedan restaurantes trabajando
                for (Thread thread : hilosRestaurante) {
                    thread.interrupt();
                }

            }

        }

        imprimirDatos(interrumpido, etapaActual, restaurantesPendientes);

    }

    /**
     * Imprimr la información del hilo
     *
     * @return Cadena de texto con la información
     */
    private void imprimirDatos(boolean interrumpido, int etapaInt, int restaurantesPendientes) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nRepartidor ").append(iD).append(" ");

        if (interrumpido) {
            mensaje.append("\"INTERRUMPIDO\"");
            mensaje.append(" en la etapa ").append(etapaInt).append(", en la cual quedaban ").append(restaurantesPendientes).append(" restaurantes pendientes de entregar un plato");
        }

        for (MenuReparto pedido : pedidos) {
            mensaje.append("\n\t").append(pedido.toString());
        }

        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }

}
