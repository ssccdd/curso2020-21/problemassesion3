/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo4;

import es.uja.ssccdd.curso2021.problemassesion3.grupo4.Utils.TipoPlato;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo4.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo4.Utils.TIEMPO_ESPERA_MAX;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo4.Utils.TIEMPO_ESPERA_MIN;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Restaurante implements Runnable {
    
    private final int iD;
    private final Utils.TipoPlato tipoPlato;
    private final CountDownLatch sincronizador;
    private final MenuReparto menu;
    
    public Restaurante(int iD, TipoPlato tipoPlato, CountDownLatch sincronizador, MenuReparto menu) {
        this.iD = iD;
        this.tipoPlato = tipoPlato;
        this.sincronizador = sincronizador;
        this.menu = menu;
    }
    
    @Override
    public void run() {
        
        int tiempoEspera = random.nextInt(TIEMPO_ESPERA_MAX - TIEMPO_ESPERA_MIN) + TIEMPO_ESPERA_MIN;
        
        try {
            TimeUnit.SECONDS.sleep(tiempoEspera);
            Plato plato = new Plato(Math.abs(random.nextInt()), tipoPlato);
            menu.addPlato(plato);
        } catch (InterruptedException ex) {
            //Este hilo no tiene que hacer nada si es interrumpido.
        }finally{
            sincronizador.countDown();
        }     
        
    }
    
}
