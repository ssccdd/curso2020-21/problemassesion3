/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo4;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo4.Utils.MENUS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo4.Utils.REPARTIDORES_A_GENERAR;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo4.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;
import java.util.concurrent.Phaser;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion3 {

    public static void main(String[] args) {
        // Variables aplicación
        int idMenus = 0;
        int idRepartidores = 0;

        ArrayList<Thread> hilosRepartidores = new ArrayList<>();

        Phaser phaserRepartidores = new Phaser(REPARTIDORES_A_GENERAR);

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        for (int i = 0; i < REPARTIDORES_A_GENERAR; i++) {

            // Inicializamos los menús
            ArrayList<MenuReparto> listaPedidos = new ArrayList<>();
            for (int j = 0; j < MENUS_A_GENERAR; j++) {
                listaPedidos.add(new MenuReparto(idMenus++));
            }

            // Inicializamos los repartidores
            Repartidor repartidor = new Repartidor(idRepartidores++, listaPedidos, phaserRepartidores);
            Thread thread = new Thread(repartidor, "Repartidor" + i);
            thread.start();
            hilosRepartidores.add(thread);

        }

        System.out.println("HILO-Principal Espera a la finalización de los repartidores");

        try {
            TimeUnit.SECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion3.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Solicita la finalización de los repartidores");

        for (Thread hilo : hilosRepartidores) {
            hilo.interrupt();
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");
    }

}
