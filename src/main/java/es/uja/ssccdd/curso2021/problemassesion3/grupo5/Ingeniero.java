/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo5.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo5.Utils.TIEMPO_REQUERIDO_MAX;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo5.Utils.TIEMPO_REQUERIDO_MIN;
import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ingeniero implements Runnable {

    private final int iD;
    private final CyclicBarrier barreraCiclo;
    private final ArrayList<Impresora3D> impresoras;
    private final ArrayList<Modelo> modelosImpresos;
    private final ReentrantLock bloqueoModelosImpresos;

    public Ingeniero(int iD, CyclicBarrier barreraCiclo, ArrayList<Impresora3D> impresoras, ArrayList<Modelo> modelosImpresos, ReentrantLock bloqueoModelosImpresos) {
        this.iD = iD;
        this.barreraCiclo = barreraCiclo;
        this.impresoras = impresoras;
        this.modelosImpresos = modelosImpresos;
        this.bloqueoModelosImpresos = bloqueoModelosImpresos;
    }

    @Override
    public void run() {

        System.out.println("El ingeniero " + iD + " ha comenzado el trabajo.");
        //Estas variables son solo para guardar el estado en la interrupciçon y mostrar los datos correctos.
        int modelosProcesados = 0;
        int ciclos = 0;
        boolean interrumpido = false;

        while (!interrumpido) {

            ciclos++;
            ArrayList<Modelo> modelosTemporales = new ArrayList<>();

            for (int i = 0; i < impresoras.size() && !interrumpido; i++) {
                try {

                    TimeUnit.MILLISECONDS.sleep(random.nextInt(TIEMPO_REQUERIDO_MAX - TIEMPO_REQUERIDO_MIN) + TIEMPO_REQUERIDO_MIN);

                    Modelo modeloAux = new Modelo(iD * 1000 + modelosProcesados, impresoras.get(i).getCalidadImpresion());
                    modelosTemporales.add(modeloAux);
                    modelosProcesados++;

                } catch (InterruptedException e) {
                    interrumpido = true;
                }

            }

            bloqueoModelosImpresos.lock();
            {//Las llaves no son obligatorias, solo las uso para resaltar la sección crítica
                modelosImpresos.addAll(modelosTemporales);
            }
            bloqueoModelosImpresos.unlock();

            System.out.println(Thread.currentThread().getName() + " ha terminado el ciclo " + ciclos + " y queda a la espera");
            try {
                barreraCiclo.await();
            } catch (Exception ex) {
                interrumpido = true;
            }

        }

        imprimirInfo(modelosProcesados, ciclos, modelosProcesados % impresoras.size());

    }

    /**
     * Imprime la información de la clase
     */
    private void imprimirInfo(int totalModelos, int ciclos, int modelosUltimoCiclo) {
        StringBuilder mensaje = new StringBuilder();

        mensaje.append("\nIngeniero ").append(iD).append(" ").append(" interrumpido en el ciclo ").append(ciclos).append(" cuando había terminado de imprimir ").append(modelosUltimoCiclo).append(" modelos.");
        mensaje.append("\nTotal de modelos procesados: ").append(totalModelos);

        System.out.println(mensaje.toString());
    }

}
