/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo5.Utils.TIEMPO_ESPERA_HILO_POSPROCESO;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class MaquinaPostProcesado implements Runnable {

    private ArrayList<Modelo> modelosImpresos;
    private ArrayList<Modelo> modelosFinalizados;
    private ReentrantLock bloqueoModelosImpresos;

    public MaquinaPostProcesado(ArrayList<Modelo> modelosImpresos, ArrayList<Modelo> modelosFinalizados, ReentrantLock bloqueoModelosImpresos) {
        this.modelosImpresos = modelosImpresos;
        this.modelosFinalizados = modelosFinalizados;
        this.bloqueoModelosImpresos = bloqueoModelosImpresos;
    }

    @Override
    public void run() {

        System.out.println("********Maquina de postprocesado iniciada********");

        int modelosProcesados = 0;

        while (!modelosImpresos.isEmpty()) {

            Modelo aProcesar;

            bloqueoModelosImpresos.lock();
            { //Las llaves no son obligatorias, solo las uso para resaltar la sección crítica
                aProcesar = modelosImpresos.get(0);
                modelosImpresos.remove(0);
            }
            bloqueoModelosImpresos.unlock();

            try {
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_HILO_POSPROCESO);
            } catch (InterruptedException ex) {
                //Este hilo no se interrumpirá por lo que no necesita tratamiento de la excepción
            }

            modelosFinalizados.add(aProcesar);
            modelosProcesados++;
        }

        System.out.println("********Maquina de postprocesado finalizada, procesados " + modelosProcesados + " modelos********");

    }

}
