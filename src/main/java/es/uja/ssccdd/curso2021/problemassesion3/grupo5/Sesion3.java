/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo5;

import es.uja.ssccdd.curso2021.problemassesion3.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo5.Utils.INGENIEROS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo5.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo5.Utils.IMPRESORAS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo5.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;

import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion3 {

    public static void main(String[] args) {

        // Variables aplicación
        int idImpresoras = 0;
        int idIngenieros = 0;

        ArrayList<Thread> hilosIngenieros = new ArrayList<>();
        ArrayList<Modelo> modelosImpresos = new ArrayList<>();
        ArrayList<Modelo> modelosFinalizados = new ArrayList<>();
        ReentrantLock bloqueoModelosImpresos = new ReentrantLock();
        MaquinaPostProcesado maquinaPostProcesado = new MaquinaPostProcesado(modelosImpresos, modelosFinalizados, bloqueoModelosImpresos);
        CyclicBarrier barreraCiclo = new CyclicBarrier(INGENIEROS_A_GENERAR, maquinaPostProcesado);

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        for (int i = 0; i < INGENIEROS_A_GENERAR; i++) {

            // Inicializamos las impresoras
            ArrayList<Impresora3D> listaImpresoras = new ArrayList<>();
            for (int j = 0; j < IMPRESORAS_A_GENERAR; j++) {
                int aleatorioCalidad = Utils.random.nextInt(VALOR_GENERACION);
                listaImpresoras.add(new Impresora3D(idImpresoras++, CalidadImpresion.getCalidad(aleatorioCalidad)));
            }

            // Inicializamos los ingenieros
            Ingeniero ingeniero = new Ingeniero(idIngenieros++, barreraCiclo, listaImpresoras, modelosImpresos, bloqueoModelosImpresos);
            Thread thread = new Thread(ingeniero, "Ingeniero" + i);
            thread.start();
            hilosIngenieros.add(thread);

        }

        System.out.println("HILO-Principal Espera a la finalización de los trabajadores");

        try {
            TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion3.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Solicita la finalización de los trabajadores");

        for (Thread thread : hilosIngenieros) {
            thread.interrupt();
        }

        System.out.println("HILO-Principal Espera a los trabajadores");
        for (Thread thread : hilosIngenieros) {
            try {
                thread.join();
            } catch (InterruptedException ex) {
                //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            }
        }

        Thread hiloMaquina = new Thread(maquinaPostProcesado);
        hiloMaquina.start();
        try {
            hiloMaquina.join();
        } catch (InterruptedException ex) {
                //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
        }
        
        
        System.out.println("HILO-Principal Postprocesados " + modelosFinalizados.size() + " modelos.");
        for (Modelo modelo : modelosFinalizados) {
            System.out.println(modelo.toString());
        }
        

        System.out.println("HILO-Principal Ha finalizado la ejecución");
    }

}
