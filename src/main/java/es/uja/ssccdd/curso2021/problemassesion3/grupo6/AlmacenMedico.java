/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion3.grupo6.Utils.TIEMPO_ESPERA_FABRICANTE;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo6.Utils.FabricanteVacuna;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo6.Utils.random;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class AlmacenMedico implements Runnable {

    private final int iD;
    private final FabricanteVacuna fabricante;
    private final CountDownLatch bloqueoEntrega;
    private final ArrayList<DosisVacuna> dosis;
    private final int dosisAGenerar;

    public AlmacenMedico(int iD, FabricanteVacuna fabricante, CountDownLatch bloqueoEntrega, ArrayList<DosisVacuna> dosis, int dosisAGenerar) {
        this.iD = iD;
        this.fabricante = fabricante;
        this.bloqueoEntrega = bloqueoEntrega;
        this.dosis = dosis;
        this.dosisAGenerar = dosisAGenerar;
    }
    
    

    @Override
    public void run() {
        try {
           
            for (int i = 0; i < dosisAGenerar; i++) {
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_FABRICANTE);
                
                dosis.add(new DosisVacuna(Math.abs(random.nextInt()), fabricante));
                bloqueoEntrega.countDown();
            }
            
        } catch (InterruptedException ex) {
            // No se trata la excepción, el hilo termina al interrumpirse 
            //y no es necesario realizar mas acciones
        }
    }
    
}
