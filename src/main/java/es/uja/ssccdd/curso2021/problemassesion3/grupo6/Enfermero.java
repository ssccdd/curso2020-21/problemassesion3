/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo6;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo6.Utils.ESPERA_ENTRE_DOSIS;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo6.Utils.FabricanteVacuna;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Enfermero implements Runnable {

    private final int iD;
    private final ArrayList<Paciente> pacientes;
    private final FabricanteVacuna fabricante;
    private final CyclicBarrier barreraEnfermeros;

    public Enfermero(int iD, ArrayList<Paciente> pacientes, FabricanteVacuna fabricante, CyclicBarrier barreraEnfermeros) {
        this.iD = iD;
        this.pacientes = pacientes;
        this.fabricante = fabricante;
        this.barreraEnfermeros = barreraEnfermeros;

    }

    @Override
    public void run() {

        //Estas variables son solo para guardar el estado en la interrupción y mostrar los datos correctos.
        ArrayList<DosisVacuna> dosis = new ArrayList<>();
        Thread hiloAlmacen = null;
        boolean interrumpido = false;
        int faseActual = 0;

        try {
            System.out.println(Thread.currentThread().getName() + ", esperando al resto de enfermeros para empezar.");
            barreraEnfermeros.await();

            for (int i = 0; i < 2; i++) {
                faseActual++;

                CountDownLatch contadorEntrega = new CountDownLatch(pacientes.size());
                AlmacenMedico almacen = new AlmacenMedico(iD, fabricante, contadorEntrega, dosis, pacientes.size());
                hiloAlmacen = new Thread(almacen);
                hiloAlmacen.start();

                System.out.println(Thread.currentThread().getName() + " ha iniciado la fase " + faseActual + ", esperando al almacén.");
                contadorEntrega.await();

                System.out.println(Thread.currentThread().getName() + " el almacén ha terminado, empezando vacunación de la fase " + faseActual + ".");

                for (int j = 0; j < pacientes.size(); j++) {
                //Es importante no iterar el vector que va a ser modificado, 
                //y sabemos que tiene el mismo tamaño que el de pacientes

                    pacientes.get(j).addDosisVacuna(dosis.get(0));
                    dosis.remove(0);

                    TimeUnit.MILLISECONDS.sleep(ESPERA_ENTRE_DOSIS);

                }

                if (faseActual < 2) {//Si es la última fase
                    System.out.println(Thread.currentThread().getName() + " ha finalizado la fase" + faseActual + ", esperando al resto.");
                    barreraEnfermeros.await();
                } else {
                    System.out.println(Thread.currentThread().getName() + " ha finalizado la fase" + faseActual + ", no va a esperar.");
                }

            }

        } catch (InterruptedException ex) {
            interrumpido = true;
        } catch (BrokenBarrierException ex) {
            interrumpido = true;
            //Este estado no debe de alcanzarse, pero por si acaso
        }

        //Por si el almacen se ha quedado trabajando
        if (interrumpido && hiloAlmacen != null && hiloAlmacen.isAlive()) {
            hiloAlmacen.interrupt();
        }

        imprimirInfo(interrumpido, faseActual, dosis.size());

    }

    /**
     * Imprime una cadena con la información de la clase
     *
     * @return Cadena de texto con la información
     */
    public void imprimirInfo(boolean interrumpido, int fase, int dosisRestantes) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\n\nEnfermero ").append(iD).append(" ");

        if (interrumpido) {
            mensaje.append("\"INTERRUMPIDO\" ");
        }

        mensaje.append("con ").append(pacientes.size()).append(" pacientes, han quedado ").append(dosisRestantes).append(" dosis en la fase ").append(fase).append(".");

        for (Paciente pac : pacientes) {
            mensaje.append("\n\t").append(pac.toString());
        }

        System.out.println(mensaje.toString());
    }

}
