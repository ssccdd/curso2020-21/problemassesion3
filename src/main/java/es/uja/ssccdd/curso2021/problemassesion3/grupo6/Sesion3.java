/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo6;

import es.uja.ssccdd.curso2021.problemassesion3.grupo6.Utils.FabricanteVacuna;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo6.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo6.Utils.PACIENTES_A_GENERAR_MAX;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo6.Utils.PACIENTES_A_GENERAR_MIN;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo6.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion3.grupo6.Utils.ENFERMEROS_A_GENERAR;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion3 {

    public static void main(String[] args) {

        // Variables aplicación
        int idEnfermeros = 0;
        int idPacientes = 0;

        ArrayList<Thread> hiloEnfermeros = new ArrayList<>();
        CyclicBarrier barreraEnfermeros = new CyclicBarrier(ENFERMEROS_A_GENERAR);

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        for (int i = 0; i < ENFERMEROS_A_GENERAR; i++) {

            // Inicializamos los pacientes
            ArrayList<Paciente> listaPacientes = new ArrayList<>();
            int pacientesAGenerar = random.nextInt(PACIENTES_A_GENERAR_MAX - PACIENTES_A_GENERAR_MIN) + PACIENTES_A_GENERAR_MIN;
            for (int j = 0; j < pacientesAGenerar; j++) {
                listaPacientes.add(new Paciente(idPacientes++));
            }
            
            FabricanteVacuna fabricante = FabricanteVacuna.getFabricante(random.nextInt(VALOR_GENERACION));

            // Inicializamos los enfermeros
            Enfermero enfermero = new Enfermero(idEnfermeros++, listaPacientes, fabricante, barreraEnfermeros);
            Thread thread = new Thread(enfermero);
            thread.start();
            hiloEnfermeros.add(thread);

        }

        System.out.println("HILO-Principal Espera a la finalización de los trabajadores");

        try {
            TimeUnit.MILLISECONDS.sleep(14000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion3.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Solicita la finalización de los trabajadores");

        for (int i = 0; i < ENFERMEROS_A_GENERAR; i++) {
            hiloEnfermeros.get(i).interrupt();
        }


        System.out.println(                "HILO-Principal Ha finalizado la ejecución");
    }

}
