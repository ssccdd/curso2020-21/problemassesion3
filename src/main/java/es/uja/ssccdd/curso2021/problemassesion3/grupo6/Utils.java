/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion3.grupo6;

import java.util.Random;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Utils {

    public static Random random = new Random();

    // Constantes del problema
    public static final int ENFERMEROS_A_GENERAR = 12;
    public static final int PACIENTES_A_GENERAR_MIN = 5;
    public static final int PACIENTES_A_GENERAR_MAX = 8;
    public static final int ESPERA_ENTRE_DOSIS = 1000;
    public static final int VALOR_GENERACION = 101; // Valor máximo
    public static final int TOTAL_CALIDADES = FabricanteVacuna.values().length;
    public static final int TIEMPO_ESPERA_HILO_PRINCIPAL = 20000;
    public static final int TIEMPO_ESPERA_FABRICANTE = 200;

    //Enumerado para el fabricante de la vacuna
    public enum FabricanteVacuna {
        PFINOS(40), ANTIGUA(80), ASTROLUNAR(100);

        private final int valor;

        private FabricanteVacuna(int valor) {
            this.valor = valor;
        }

        /**
         * Obtenemos un tipo de vacuna relacionada con su valor de generación
         *
         * @param valor, entre 0 y 100, de generación
         * @return el laboratorio asociado con el valor de generación
         */
        public static FabricanteVacuna getFabricante(int valor) {
            FabricanteVacuna resultado = null;
            FabricanteVacuna[] laboratorios = FabricanteVacuna.values();
            int i = 0;

            while ((i < laboratorios.length) && (resultado == null)) {
                if (laboratorios[i].valor >= valor) {
                    resultado = laboratorios[i];
                }

                i++;
            }

            return resultado;
        }
    }

}
